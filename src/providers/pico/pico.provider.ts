import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import {Pico} from "../../models/pico";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";

const path = "picos";

@Injectable()
export class PicoProvider {
  constructor( public http : HttpClient) {
  }

  public adicionar(body) {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/inserir`, body, {
      observe: 'response',
      responseType: 'text'
    });
  }

  public listar(): Observable<Pico[]> {
    return this.http.get<Pico[]>(`${API_CONFIG.baseUrl}/${path}/listar`);
  }
  public findById(id: string) : Observable<Pico>{
    return this.http.get<Pico>(`${API_CONFIG.baseUrl}/${path}/${id}`);
  }
}
