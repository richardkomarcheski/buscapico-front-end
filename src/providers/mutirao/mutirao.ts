import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpConfigProvider} from "../http-config/http-config";
import {Mutirao} from "../../models/mutirao";
import {Anuncio} from "../../models/anuncio";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";

/*
  Generated class for the MutiraoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const path = "mutiraos";
@Injectable()
export class MutiraoProvider {

  constructor(public Http: HttpClient) {
  }
  public adicionar(body) {
    return this.Http.post(`${API_CONFIG.baseUrl}/${path}/inserir`, body,{
      observe: 'response',
      responseType: 'text'
    });
  }

  public listar(): Observable<Mutirao[]> {
    return this.Http.get<Mutirao[]>(`${API_CONFIG.baseUrl}/${path}/listar`);
  }
  public deleteById(id: string) : Observable<Mutirao> {
    return this.Http.delete<Mutirao>(`${API_CONFIG.baseUrl}/${path}/${id}/excluir`);
  }

  public editar(body, id : string): Observable<Mutirao> {
    return this.Http.put<Mutirao>(`${API_CONFIG.baseUrl}/${path}/${id}/editar`, body);
  }

  public findById(id: string) : Observable<Mutirao>{
    return this.Http.get<Mutirao>(`${API_CONFIG.baseUrl}/${path}/${id}`);
  }
}
