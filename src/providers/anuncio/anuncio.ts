import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpConfigProvider} from "../http-config/http-config";
import {Anuncio} from "../../models/anuncio";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";
import {Comentario} from "../../models/comentario";


const path = "anuncios";
@Injectable()

export class AnuncioProvider {

  constructor(public http: HttpClient) {
  }
  public adicionar(body) {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/inserir`, body, {
      observe: 'response',
      responseType: 'text'
    });
  }

  public listar(): Observable<Anuncio[]> {
    return this.http.get<Anuncio[]>(`${API_CONFIG.baseUrl}/${path}/listar`);
  }

  public deleteById(id: string) : Observable<Anuncio> {
    return this.http.delete<Anuncio>(`${API_CONFIG.baseUrl}/${path}/${id}/excluir`);
  }

  public editar(body, id : string): Observable<Anuncio> {
    return this.http.put<Anuncio>(`${API_CONFIG.baseUrl}/${path}/${id}/editar`, body);
  }

  public findById(anuncio_id: string) : Observable<Anuncio>{
    return this.http.get<Anuncio>(`${API_CONFIG.baseUrl}/${path}/${anuncio_id}`);
  }

  inserirComentario(body, id: string) : Observable<Comentario> {
    return this.http.put<Comentario>(`${API_CONFIG.baseUrl}/${path}/${id}/comentario`, body);
  }
}
