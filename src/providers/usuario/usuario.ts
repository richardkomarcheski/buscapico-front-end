import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";
import {Usuario} from "../../models/usuario";
import {StorageService} from "../storage/storage.service";


const path = "usuarios";

@Injectable()
export class UsuarioProvider {
  constructor(public http: HttpClient, public storage: StorageService) {
  }

  public adicionar(body) {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/cadastrar`, body,
      {
        observe: 'response',
        responseType: 'text'
      });
  }

  findByEmail(email: string): Observable<Usuario> {
    return this.http.get<Usuario>(`${API_CONFIG.baseUrl}/${path}/email?value=${email}`);
  }
}
