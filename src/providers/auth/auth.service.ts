import {Injectable} from "@angular/core";
import {Credencias} from "../../models/credencias";
import {Observable} from "rxjs";
import {LocalUser} from "../../models/local_user";
import {StorageService} from "../storage/storage.service";
import {JwtHelper} from "angular2-jwt";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";


const path = "usuarios";
@Injectable()
export class AuthService {

  jwtHelper: JwtHelper = new JwtHelper();

  constructor(public http: HttpClient, public storageService: StorageService) {
  }

  authenticate(creds: Credencias) {
    return this.http.post(`${API_CONFIG.baseUrl}/login`, creds,
      {
        observe: 'response',
        responseType: 'text'
      });
  }

  refreshToken() {
    return this.http.post(`${API_CONFIG.baseUrl}/auth/refresh_token`,{},
      {
        observe: 'response',
        responseType: 'text'
      });
  }

  sucessfulLogin(authorizationValue: string) {
    let tok = authorizationValue.substring(7);
    let user: LocalUser = {
      token: tok,
      email: this.jwtHelper.decodeToken(tok).sub
    };
    this.storageService.setLocalUser(user);
  }

  logout() {
    this.storageService.setLocalUser(null);
  }
}
