import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Platform } from "ionic-angular";
import { BehaviorSubject } from "rxjs";

const TOKEN_KEY = "auth-token";

@Injectable()
export class AuthProvider {
  authenticationState = new BehaviorSubject(false);
  constructor(private storage: Storage, private platform: Platform) {
    console.log("Hello AuthProvider Provider");
    this.platform.ready().then(() => {
      this.storage.get(TOKEN_KEY).then(res => {
        if (res) {
          this.authenticationState.next(true);
        }
      });
    });
  }
  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  login() {
    return this.storage.set(TOKEN_KEY, "Bearer 1234567").then(() => {
      this.authenticationState.next(true);
    });
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }
}
