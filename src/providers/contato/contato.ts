import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";
import {Observable} from "rxjs";
import {Contato} from "../../models/contato";

const path = "contatos";
@Injectable()

export class ContatoProvider {

  constructor(public http: HttpClient) {
  }
  public adicionar(body, usuario : string) {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/${usuario}/inserir`, body,{
      observe: 'response',
      responseType: 'text'
    });
  }

  public listar(email: string): Observable<Contato[]> {
    return this.http.get<Contato[]>(`${API_CONFIG.baseUrl}/${path}/listar/email?value=${email}`);
  }

  public deleteById( email : string, id : string) :Observable<Contato> {
    return this.http.delete<Contato>(`${API_CONFIG.baseUrl}/${path}/${email}/${id}/excluir`);
  }

  public editar(body, id : string): Observable<Contato> {
    return this.http.put<Contato>(`${API_CONFIG.baseUrl}/${path}/${id}/editar`, body);
  }

  public findById(email :string , id : string) : Observable<Contato> {
    return this.http.get<Contato>(`${API_CONFIG.baseUrl}/${path}/detalhes/${email}/${id}`);
  }
}
