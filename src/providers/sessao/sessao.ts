import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Sessao} from "../../models/sessao";
import {HttpClient} from "@angular/common/http";
import {API_CONFIG} from "../../config/api.config";
import {Comentario} from "../../models/comentario";
import {Video} from "../../models/video";
import {Foto} from "../../models/foto";




const path = "sessaos";
@Injectable()
export class SessaoProvider {

  constructor(public http: HttpClient) {
    console.log('Hello SessaoProvider Provider');
  }

  public adicionar(body) {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/inserir`, body, {
      observe: 'response',
      responseType: 'text'
    });
  }

  public listar(): Observable<Sessao[]> {
    return this.http.get<Sessao[]>(`${API_CONFIG.baseUrl}/${path}/listar`);
  }
  public findById(id: string) : Observable<Sessao>{
    return this.http.get<Sessao>(`${API_CONFIG.baseUrl}/${path}/${id}`);
  }
  public deleteById(id: string) : Observable<Sessao> {
    return this.http.delete<Sessao>(`${API_CONFIG.baseUrl}/${path}/${id}/excluir`);
  }

  public editar(body, id : string): Observable<Sessao> {
      return this.http.put<Sessao>(`${API_CONFIG.baseUrl}/${path}/${id}/editar`, body);
  }

  inserirVideo(body, id: string): Observable<Video>  {
    return this.http.put<Video>(`${API_CONFIG.baseUrl}/${path}/${id}/video`, body);
  }

  inserirFoto(body, id: string): Observable<Foto> {
    return this.http.put<Foto>(`${API_CONFIG.baseUrl}/${path}/${id}/foto`, body);
  }

  inserirComentario(body, id: string) : Observable<Comentario> {
    return this.http.put<Comentario>(`${API_CONFIG.baseUrl}/${path}/${id}/comentario`, body);
  }
}
