import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {IonicPage, MenuController, NavController, NavParams} from "ionic-angular";
import {Credencias} from "../../../models/credencias";
import {AuthService} from "../../../providers/auth/auth.service";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  protected formLogin: FormGroup;

  creds : Credencias = {
    email: "",
    senha: ""
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public authService : AuthService,
    public menuCtrl : MenuController
  ) {}

  ngOnInit() {
    this.initFormLogin();
  }

  ionViewDidEnter(){
    this.authService.refreshToken().subscribe(response =>{
      this.authService.sucessfulLogin(response.headers.get('Authorization'));
      this.navCtrl.setRoot("ListarPicosPage");
      this.menuCtrl.enable(true);
      this.menuCtrl.swipeEnable(true);
    },error => {
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
    });
  }

  private initFormLogin(): void {
    this.formLogin = this.formBuilder.group({
      email: ["", [Validators.required]],
      senha: ["", Validators.required]
    });
  }

  protected doLogin(): void {
      this.authService.authenticate(this.creds).subscribe(response =>{
      this.authService.sucessfulLogin(response.headers.get('Authorization'));
      this.navCtrl.setRoot("ListarPicosPage");
      this.menuCtrl.enable(true);
      },error => {});
  }

  cadastrarUsuario() {
    this.navCtrl.push("CadastroPage");
  }
}
