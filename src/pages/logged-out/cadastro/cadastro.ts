import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, MenuController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsuarioProvider} from "../../../providers/usuario/usuario";
import {StorageService} from "../../../providers/storage/storage.service";

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {
  private formCadastro: FormGroup;
  private dataFormato = new Date().toISOString();

  constructor(
    private  loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private usuarioProvider: UsuarioProvider,
    public menuCtrl: MenuController,
    public storageService: StorageService
  ) {
  }

  ngOnInit() {
    this.initForm();
    // this.storageService.setLocalUser(null);
    this.menuCtrl.swipeEnable(false);

  }


  private initForm() {
    this.formCadastro = this.formBuilder.group({
      nome: ["", Validators.required],
      email: ["", Validators.required],
      senha: ["", Validators.required],
      dataNascimento: ["", Validators.required],
    });
  }


  cadastrarUsuario() {
    let formValues = this.formCadastro.getRawValue();
    let {nome, email, senha, dataNascimento} = formValues;
    let body = {
      nome,
      email,
      senha,
      dataNascimento
    };
    this.usuarioProvider.adicionar(body).subscribe(response => {
        this.adicionarSucesso();
      },
      error => {
      }
    );
  }

  adicionarSucesso() {
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'usuário cadastrado com sucesso!',
      enableBackdropDismiss: false,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.pop();
        }
      }
      ]
    });
    alert.present();
  }
}
