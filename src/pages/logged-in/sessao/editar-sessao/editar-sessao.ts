import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Sessao} from "../../../../models/sessao";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Pico} from "../../../../models/pico";
import {Contato} from "../../../../models/contato";
import {PicoProvider} from "../../../../providers/pico/pico.provider";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {StorageService} from "../../../../providers/storage/storage.service";

/**
 * Generated class for the EditarSessaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-sessao',
  templateUrl: 'editar-sessao.html',
})
export class EditarSessaoPage {
  item: Sessao = this.navParams.get('sessao');
  dataCriacao = new Date().toISOString();
  formSessao: FormGroup;
  id: string = this.navParams.get('id');
  contatos: Contato[];
  picos: Pico[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public sessaoProvider: SessaoProvider,
    public picoProvider: PicoProvider,
    public contatoProvider: ContatoProvider,
    public storage: StorageService
  ) {
    this.formSessao = this.formBuilder.group({
      nome: [,Validators.required],
      dataSessao: [,Validators.required],
      dataCadastro: [],
      pico: [,Validators.required],
      participantes: [,Validators.required]
    });
  }

  ionViewDidLoad() {
    this.picoProvider.listar().subscribe(
      response => {
        this.picos = response;
        this.formSessao.controls.pico.setValue(this.picos[0].nome);
      },
      error => {
      }
    );
    this.contatoProvider.listar(this.storage.getLocalUser().email).subscribe(
      response => {
        this.contatos = response;
        this.formSessao.controls.participantes.setValue(this.contatos[0].nome);
      },
      error => {
      }
    );

  }

  ngOnInit() {
    this.initForm();
  console.log(this.item);
  }



  private initForm() {

    this.formSessao = this.formBuilder.group({
      nome: [this.item.nome, Validators.required],
      dataSessao: [this.item.dataSessao, Validators.required],
      dataCadastro: [],
      pico: [,Validators.required],
      participantes: [,Validators.required]
    });


  }


  salvarSessao() {
    let formValues = this.formSessao.getRawValue();
    console.log(formValues);

    let {nome, dataSessao, dataCadastro, pico,
      participantes} = formValues;
    let body = {
      nome,
      dataSessao,
      dataCadastro,
      pico,
      participantes
    };
    body.dataCadastro = this.item.dataCadastro;

    this.sessaoProvider.editar(body, this.id).subscribe(response => {

        this.navCtrl.pop();
      },
      error => {
      }
    );
  }
}
