import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriarSessaoPage } from './criar-sessao';

@NgModule({
  declarations: [
    CriarSessaoPage,
  ],
  imports: [
    IonicPageModule.forChild(CriarSessaoPage),
  ],
})
export class CriarSessaoPageModule {}
