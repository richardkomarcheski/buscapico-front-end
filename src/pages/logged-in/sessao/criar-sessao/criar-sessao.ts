import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Pico} from "../../../../models/pico";
import {PicoProvider} from "../../../../providers/pico/pico.provider";
import {Contato} from "../../../../models/contato";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {StorageService} from "../../../../providers/storage/storage.service";
import {Sessao} from "../../../../models/sessao";
import {convertValueToOutputAst} from "@angular/compiler/src/output/value_util";

/**
 * Generated class for the CriarSessaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-sessao',
  templateUrl: 'criar-sessao.html',
})
export class CriarSessaoPage {
  picos: Pico[];
  contatos: Contato[];
  contatosAux : Contato[];
  sessao : Sessao;
  formSessao : FormGroup;
  dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public sessaoProvider: SessaoProvider,
    public alertCtrl: AlertController,
    public picoProvider: PicoProvider,
    public contatoProvider: ContatoProvider,
    public storage: StorageService
  ) {
    this.formSessao = this.formBuilder.group({
      nome: [, Validators.required],
      dataSessao: [, Validators.required],
      dataCadastro: [],
      pico: [,],
      participantes: [,]
    });

  }


  ionViewDidLoad() {
    this.picoProvider.listar().subscribe(
      response => {
        this.picos = response;
        this.formSessao.controls.pico.setValue(this.picos[0].nome);
      },
      error => {
      }
    );
    this.contatoProvider.listar(this.storage.getLocalUser().email).subscribe(
      response => {
        this.contatos = response;
        this.formSessao.controls.participantes.setValue(this.contatos[0].nome);
      },
      error => {
      }
    );
  }

  adicionarSessao() {
    let formValues = this.formSessao.getRawValue();


    let {nome, dataSessao, dataCadastro, pico, participantes} = formValues;
    let body = {
      nome,
      dataSessao,
      dataCadastro,
      pico,
      participantes
    };
    body.dataCadastro = this.dataCriacao;



    this.sessaoProvider.adicionar(body).subscribe(response => {
        this.adicionadoSucesso();
      },
      error => {
      }
    );
  }

  adicionadoSucesso() {

    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Sessão cadastrada com sucesso!',
      enableBackdropDismiss: false,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.pop();
        }
      }
      ]
    });
    alert.present();
  }
}

