import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarSessaoPage } from './listar-sessao';
import {PipesModule} from "../../../../pipes/pipes.module";

@NgModule({
  declarations: [
    ListarSessaoPage
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(ListarSessaoPage),
  ],
})
export class ListarSessaoPageModule {}
