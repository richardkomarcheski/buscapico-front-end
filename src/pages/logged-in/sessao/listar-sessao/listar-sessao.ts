import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Sessao} from "../../../../models/sessao";

/**
 * Generated class for the ListarSessaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listar-sessao',
  templateUrl: 'listar-sessao.html',
})
export class ListarSessaoPage {
  items : Sessao [] ;
  descending: boolean = false;
  order: number;
  column: string = 'name';
  terms: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl : LoadingController,
              public sessaoProvider : SessaoProvider) {

  }

  ionViewDidLoad() {
    this.loadData();
  }
  ionViewDidEnter() {
    this.loadData();
  }

  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  loadData() {

    let loader = this.presentLoading();
    this.sessaoProvider.listar()
      .subscribe(response => {
          this.items = response;
              loader.dismiss();
        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  cadastrarSessao() {
    this.navCtrl.push("CriarSessaoPage");
  }

  detalheSessao(id: string) {
    this.navCtrl.push("DetalhesSessaoPage", {id:id});
  }


}
