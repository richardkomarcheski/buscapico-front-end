import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Sessao} from "../../../../models/sessao";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Video} from "../../../../models/video";
import {StorageService} from "../../../../providers/storage/storage.service";
import {Usuario} from "../../../../models/usuario";
import {UsuarioProvider} from "../../../../providers/usuario/usuario";
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Foto} from "../../../../models/foto";
import {Comentario} from "../../../../models/comentario";
import {Contato} from "../../../../models/contato";
import {ImageViewerController} from "ionic-img-viewer";
import {getVideoId} from "get-video-id";



@IonicPage()
@Component({
  selector: 'page-detalhes-sessao',
  templateUrl: 'detalhes-sessao.html',
})
export class DetalhesSessaoPage {
  item: Sessao;
  video: Video;
  foto: Foto;
  comentario: Comentario;
  fotos: Foto[];
  videos: Video[];
  comentarios: Comentario[];
  participantes : Contato[];
  usuario: Usuario;
  url: SafeResourceUrl;
  id: string = this.navParams.get('id');


  _imageViewerCtrl: ImageViewerController;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sessaoProvider: SessaoProvider,
    public alertCtrl: AlertController,
    public storage: StorageService,
    public usuarioProvider: UsuarioProvider,
    public sanitizer: DomSanitizer,
    public imageViewerCtrl :  ImageViewerController
  ) {
    this._imageViewerCtrl = imageViewerCtrl;
  }


  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
    this.carregarDetalhes();
  }

  carregarDetalhes() {

    let id = this.navParams.get('id');
    this.sessaoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          this.videos = this.item.videos;
          this.fotos = this.item.fotos;
          this.comentarios = this.item.comentarios;
          this.participantes = this.item.participantes;
        },
        error => {
        });
    this.carregarUsuario();
  }


  editarSessao(sessao: Sessao, id: string) {
    this.navCtrl.push("EditarSessaoPage", {sessao: sessao, id: id});
  }

  excluirSessao() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão de sessão',
      message: 'Você realmente deseja excluir essa sessão?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.sessaoProvider.deleteById(this.id).subscribe(response => {
                this.item = response;
                this.navCtrl.pop();
              },
              error => {
              });
          }
        }
      ]
    })
    alert.present();
  }

  adicionarVideo() {
    let alert = this.alertCtrl.create({
      title: 'Adicionar Vídeo',
      message: 'coloque a Url do vídeo do yotube',
      inputs: [
        {
          name: 'urlYoutube',
          placeholder: 'youtube.com'
        },
        {
          name: 'descricao',
          placeholder: 'descrição'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: data => {
            this.carregarUsuario();
            this.video = data;
            this.video.user = this.usuario;
            this.sessaoProvider.inserirVideo(this.video, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });

    alert.present();
  }

  adicionarFoto() {
    let alert = this.alertCtrl.create({
      title: 'Adicionar Foto',
      message: 'coloque o link da foto',
      inputs: [
        {
          name: 'path',
          placeholder: 'Link'
        },
        {
          name: 'descricao',
          placeholder: 'descrição'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: dataFoto => {
            this.carregarUsuario();
            this.foto = dataFoto;
            this.sessaoProvider.inserirFoto(this.foto, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });
    alert.present();
  }

  adicionarComentario() {
    let alert = this.alertCtrl.create({
      title: 'Adicionar Comentário',
      message: 'escreva seu comentário',
      inputs: [
        {
          name: 'texto',
          placeholder: 'Comentário'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: dataComentario => {
            this.carregarUsuario();
            this.comentario = dataComentario;
            this.comentario.usuario = this.usuario;
            this.comentario.data = new Date();
            console.log(this.comentario);
            this.sessaoProvider.inserirComentario(this.comentario, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });
    alert.present();
  }

  carregarUsuario() {
    this.usuarioProvider.findByEmail(this.storage.getLocalUser().email).subscribe(response => {
        let {id, nome, email} = response;
        this.usuario = null;
        this.usuario = {id, nome, email};
      },
      error => {
      });
  }

  popImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  urlSafe(urlYoutube: string) {
    var videoid = urlYoutube.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);

    urlYoutube = 'http://www.youtube.com/embed/' + videoid[1];

   return this.url =  this.sanitizer.bypassSecurityTrustResourceUrl(urlYoutube);

  }
}
