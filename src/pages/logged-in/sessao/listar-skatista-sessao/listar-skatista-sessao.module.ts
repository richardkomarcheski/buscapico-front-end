import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarSkatistaSessaoPage } from './listar-skatista-sessao';

@NgModule({
  declarations: [
    ListarSkatistaSessaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarSkatistaSessaoPage),
  ],
})
export class ListarSkatistaSessaoPageModule {}
