import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EventoProvider} from "../../../../providers/evento/evento";


@IonicPage()
@Component({
  selector: 'page-cadastrar-eventos',
  templateUrl: 'cadastrar-eventos.html',
})
export class CadastrarEventosPage {


  private formEvento: FormGroup;
  private dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public eventoProvider : EventoProvider
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formEvento = this.formBuilder.group({
      nome : ["", Validators.required],
      dataCadastro: [],

    });

  }

  adicionarEvento() {
    let formValues = this.formEvento.getRawValue();
    console.log(formValues);

    let {nome, dataCadastro} = formValues;
    let body = {
      nome,
        dataCadastro

    };
    body.dataCadastro = this.dataCriacao;

    console.log("body", body);

    this.eventoProvider.adicionar(body).subscribe(response => {

      },
      error => {}
    );

    this.navCtrl.pop();
  }

}
