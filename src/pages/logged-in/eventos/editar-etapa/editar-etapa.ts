import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Sessao} from "../../../../models/sessao";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Contato} from "../../../../models/contato";
import {Pico} from "../../../../models/pico";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {PicoProvider} from "../../../../providers/pico/pico.provider";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {StorageService} from "../../../../providers/storage/storage.service";
import {Etapa} from "../../../../models/etapa";
import {EventoProvider} from "../../../../providers/evento/evento";

/**
 * Generated class for the EditarEtapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-etapa',
  templateUrl: 'editar-etapa.html',
})
export class EditarEtapaPage {
  item: Etapa = this.navParams.get('etapa');
  dataCriacao = new Date().toISOString();
  formEtapa: FormGroup;
  id: string = this.navParams.get('id');
  picos: Pico[];
  contatos : Contato[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public eventoProvider: EventoProvider,
    public picoProvider: PicoProvider,
    public contatoProvider: ContatoProvider,
    public storage: StorageService
  ) {
    this.formEtapa = this.formBuilder.group({
      nome: [,Validators.required],
      data: [,Validators.required],
      pico: [,Validators.required],
      contatos: [,Validators.required]
    });
  }

  ionViewDidLoad() {
    this.picoProvider.listar().subscribe(
      response => {
        this.picos = response;
        this.formEtapa.controls.pico.setValue(this.picos[0].nome);
      },
      error => {
      }
    );
    this.contatoProvider.listar(this.storage.getLocalUser().email).subscribe(
      response => {
        this.contatos = response;
        this.formEtapa.controls.participantes.setValue(this.contatos[0].nome);
      },
      error => {
      }
    );

  }

  ngOnInit() {
    this.initForm();
    console.log(this.item);
  }



  private initForm() {

    this.formEtapa = this.formBuilder.group({
      nome: [this.item.nome, Validators.required],
      data: [this.item.data, Validators.required],
      pico: [,Validators.required],
      participantes: [,Validators.required]

    });


  }


  salvarEtapa() {
    let formValues = this.formEtapa.getRawValue();
    console.log(formValues);

    let {nome, data, pico, participantes
      } = formValues;
    let body = {
      nome,
      data,
      pico,
      participantes
    };


    this.eventoProvider.editarEtapa(body, this.id).subscribe(response => {

        this.navCtrl.pop();
      },
      error => {
      }
    );
  }

}
