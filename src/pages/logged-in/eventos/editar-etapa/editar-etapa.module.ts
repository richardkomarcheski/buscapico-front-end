import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarEtapaPage } from './editar-etapa';

@NgModule({
  declarations: [
    EditarEtapaPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarEtapaPage),
  ],
})
export class EditarEtapaPageModule {}
