import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {PicoProvider} from "../../../../providers/pico/pico.provider";
import {StorageService} from "../../../../providers/storage/storage.service";
import {Pico} from "../../../../models/pico";
import {Evento} from "../../../../models/evento";
import {Etapa} from "../../../../models/etapa";
import {EventoProvider} from "../../../../providers/evento/evento";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {Contato} from "../../../../models/contato";

/**
 * Generated class for the AdicionarEtapasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adicionar-etapas',
  templateUrl: 'adicionar-etapas.html',
})
export class AdicionarEtapasPage {
  picos: Pico[];
  contatos : Contato[];
  formEtapa: FormGroup;
  evento: Evento = this.navParams.get('evento');
  id: string = this.navParams.get('id');
  dataCriacao = new Date().toISOString();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public alertCtrl: AlertController,
              public picoProvider: PicoProvider,
              public eventoProvider: EventoProvider,
              public storage: StorageService,
              public contatoProvider : ContatoProvider) {

    this.formEtapa = this.formBuilder.group({
      nome: [, Validators.required],
      data: [, Validators.required],
      pico: [, Validators.required],
      participantes: [, Validators.required],
    });
  }

  ionViewDidLoad() {
    this.picoProvider.listar().subscribe(
      response => {
        this.picos = response;
        this.formEtapa.controls.pico.setValue(this.picos[0].nome);
      },
      error => {
      }
    );
    this.contatoProvider.listar(this.storage.getLocalUser().email).subscribe(
      response => {
        this.contatos = response;
        this.formEtapa.controls.participantes.setValue(this.contatos[0].nome);
      },
      error => {
      }
    );
  }

  salvarEtapa() {
    let formValues = this.formEtapa.getRawValue();


    let {nome, data, pico, participantes} = formValues;
    let body = {
      nome,
      data,
      pico,
      participantes
    };

    this.eventoProvider.adicionarEtapa(body, this.id).subscribe(response => {
        this.adicionadoSucesso();
        console.log(body);
      },
      error => {
      }
    );
  }


  adicionadoSucesso() {

    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Etapa cadastrada com sucesso!',
      enableBackdropDismiss: false,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.pop();
        }
      }
      ]
    });
    alert.present();
  }
}
