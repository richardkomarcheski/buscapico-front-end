import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarEtapasPage } from './adicionar-etapas';

@NgModule({
  declarations: [
    AdicionarEtapasPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarEtapasPage),
  ],
})
export class AdicionarEtapasPageModule {}
