import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarEventosPage } from './listar-eventos';

@NgModule({
  declarations: [
    ListarEventosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarEventosPage),
  ],
})
export class ListarEventosPageModule {}
