import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {EventoProvider} from "../../../../providers/evento/evento";
import {Evento} from "../../../../models/evento";
import {Video} from "../../../../models/video";
import {Foto} from "../../../../models/foto";
import {Comentario} from "../../../../models/comentario";
import {Usuario} from "../../../../models/usuario";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {ImageViewerController} from "ionic-img-viewer";
import {StorageService} from "../../../../providers/storage/storage.service";
import {UsuarioProvider} from "../../../../providers/usuario/usuario";
import {Etapa} from "../../../../models/etapa";



@IonicPage()
@Component({
  selector: 'page-detalhes-evento',
  templateUrl: 'detalhes-evento.html',
})
export class DetalhesEventoPage {

  item: Evento;
  id: string = this.navParams.get('id');
  video: Video;
  foto: Foto;
  comentario: Comentario;
  fotos: Foto[];
  videos: Video[];
  comentarios: Comentario[];
  etapas : Etapa[];
  usuario: Usuario;
  url: SafeResourceUrl;
  _imageViewerCtrl: ImageViewerController;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public eventoProvider: EventoProvider,
    public alertCtrl: AlertController,
    public storage: StorageService,
    public usuarioProvider: UsuarioProvider,
    public sanitizer: DomSanitizer,
    public imageViewerCtrl :  ImageViewerController
  ) {
    this._imageViewerCtrl = imageViewerCtrl;
    this.carregarUsuario();
  }


  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
   this.carregarDetalhes();
  }

  editarEvento(evento : Evento, id: string) {
    this.navCtrl.push("EditarEventoPage", {evento : evento, id : id});

  }

  excluirEvento() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão do Evento',
      message: 'Você realmente deseja excluir esse Evento?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.eventoProvider.deleteById(this.id).subscribe(response => {
                this.item = response;
                this.navCtrl.pop();
              },
              error => {});
          }
        }
      ]
    });
    alert.present();
  }

  private carregarDetalhes() {
    let id = this.navParams.get('id');
    this.eventoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          this.comentarios = this.item.comentarios;
          this.fotos = this.item.fotos;
          this.videos = this.item.videos;
          this.etapas = this.item.etapas;
          },
        error => {});
    this.carregarUsuario();
    console.log(this.item);
  }

  inserirEtapa(evento : Evento, id: string) {
    this.navCtrl.push("AdicionarEtapasPage", {evento : evento, id : id});
  }


  adicionarVideo() {
    let alert = this.alertCtrl.create({
      title: 'Adicionar Vídeo',
      message: 'coloque a Url do vídeo do yotube',
      inputs: [
        {
          name: 'urlYoutube',
          placeholder: 'youtube.com'
        },
        {
          name: 'descricao',
          placeholder: 'descrição'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: data => {
            this.carregarUsuario();
            this.video = data;
            this.video.user = this.usuario;
            this.eventoProvider.inserirVideo(this.video, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });

    alert.present();
  }

  adicionarFoto() {
    let alert = this.alertCtrl.create({
      title: 'Adicionar Foto',
      message: 'coloque o link da foto',
      inputs: [
        {
          name: 'path',
          placeholder: 'Link'
        },
        {
          name: 'descricao',
          placeholder: 'descrição'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: dataFoto => {
            this.carregarUsuario();
            this.foto = dataFoto;
            this.eventoProvider.inserirFoto(this.foto, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });
    alert.present();
  }

  adicionarComentario() {
    let alert = this.alertCtrl.create({
      title: 'Adicionar Comentário',
      message: 'escreva seu comentário',
      inputs: [
        {
          name: 'texto',
          placeholder: 'Comentário'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: dataComentario => {
            this.carregarUsuario();
            this.comentario = dataComentario;
            this.comentario.usuario = this.usuario;
            this.comentario.data = new Date();
            console.log(this.comentario);
            this.eventoProvider.inserirComentario(this.comentario, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });
    alert.present();
  }
  carregarUsuario() {
    this.usuarioProvider.findByEmail(this.storage.getLocalUser().email).subscribe(response => {
        let {id, nome, email} = response;
        this.usuario = null;
        this.usuario = {id, nome, email};
      },
      error => {
      });
  }

  popImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  urlSafe(urlYoutube: string) {
    var videoid = urlYoutube.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);

    urlYoutube = 'http://www.youtube.com/embed/' + videoid[1];

    return this.url =  this.sanitizer.bypassSecurityTrustResourceUrl(urlYoutube);

  }

  detalhesEtapa(id: string, evento : Evento) {
    this.navCtrl.push("DetalhesEtapaPage",{id : id, evento: evento});
  }
}
