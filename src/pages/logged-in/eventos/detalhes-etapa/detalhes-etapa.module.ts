import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesEtapaPage } from './detalhes-etapa';

@NgModule({
  declarations: [
    DetalhesEtapaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesEtapaPage),
  ],
})
export class DetalhesEtapaPageModule {}
