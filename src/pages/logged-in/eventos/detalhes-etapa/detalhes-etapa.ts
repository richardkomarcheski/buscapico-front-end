import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Anuncio} from "../../../../models/anuncio";
import {Foto} from "../../../../models/foto";
import {Comentario} from "../../../../models/comentario";
import {Usuario} from "../../../../models/usuario";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";
import {StorageService} from "../../../../providers/storage/storage.service";
import {UsuarioProvider} from "../../../../providers/usuario/usuario";
import {EventoProvider} from "../../../../providers/evento/evento";
import {Etapa} from "../../../../models/etapa";
import {Evento} from "../../../../models/evento";
import {Contato} from "../../../../models/contato";

/**
 * Generated class for the DetalhesEtapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes-etapa',
  templateUrl: 'detalhes-etapa.html',
})
export class DetalhesEtapaPage {
  item: Etapa;
  evento : Evento  = this.navParams.get('evento');
  id: string = this.navParams.get('id');
  usuario: Usuario;
  participantes : Contato[];

  constructor( public navCtrl: NavController,
               public navParams: NavParams,
               public eventoProvider: EventoProvider,
               public alertCtrl: AlertController,
               public storage: StorageService,
               public usuarioProvider: UsuarioProvider) {
  }

  ionViewDidEnter() {
    this.carregarDetalhes();
  }
  carregarDetalhes() {
    let id = this.navParams.get('id');
    this.eventoProvider.findByIdEtapa(id)
      .subscribe(response => {
          this.item = response;
          this.participantes = this.item.participantes;
        },
        error => {
        });
  }

  editarEtapa(etapa : Etapa, id: string) {
    this.navCtrl.push("EditarEtapaPage", {etapa : etapa, id : id});
  }

  excluirEtapa() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão desta Etapa',
      message: 'Você realmente deseja excluir essa Etapa?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.eventoProvider.deleteByIdEtapa(this.id).subscribe(response => {
                this.item = response;
                this.navCtrl.pop();
              },
              error => {});
          }
        }
      ]
    })
    alert.present();
  }
}
