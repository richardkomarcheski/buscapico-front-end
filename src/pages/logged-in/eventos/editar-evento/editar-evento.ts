import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Evento} from "../../../../models/evento";
import {EventoProvider} from "../../../../providers/evento/evento";


@IonicPage()
@Component({
  selector: 'page-editar-evento',
  templateUrl: 'editar-evento.html',
})
export class EditarEventoPage {

  item: Evento = this.navParams.get('evento');
  item2 : Evento;
  private formEvento: FormGroup;
  id: string = this.navParams.get('id');
  private dataCriacao = new Date().toISOString();


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public eventoProvider: EventoProvider
  ) {
  }

  ionViewDidLoad() {

  }

  ngOnInit() {
    this.initForm();
  }


  private initForm() {
    this.item2   = this.navParams.get('evento');
    this.formEvento = this.formBuilder.group({
      nome: [this.item2.nome],
      dataCadastro: [],
    });

  }


  salvarEvento() {
    let formValues = this.formEvento.getRawValue();
    console.log(formValues);

    let {nome, dataCadastro} = formValues;
    let body = {
      nome, dataCadastro
    };
    body.dataCadastro = this.item.dataCadastro;

    this.eventoProvider.editar(body, this.id).subscribe(response => {
        this.navCtrl.pop();
      },
      error => {
      }
    );
  }
}
