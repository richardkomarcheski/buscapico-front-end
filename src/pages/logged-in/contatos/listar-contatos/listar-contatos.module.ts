import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarContatosPage } from './listar-contatos';

@NgModule({
  declarations: [
    ListarContatosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarContatosPage),
  ],
})
export class ListarContatosPageModule {}
