import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Contato} from "../../../../models/contato";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {StorageService} from "../../../../providers/storage/storage.service";


@IonicPage()
@Component({
  selector: 'page-listar-contatos',
  templateUrl: 'listar-contatos.html',
})
export class ListarContatosPage {
  items : Contato [] ;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl : LoadingController,
              public contatoProvider : ContatoProvider,
                public storage: StorageService) {
  }

  ionViewDidLoad() {
    this.loadData();
  }
  ionViewDidEnter(){
    this.loadData();
  }

  loadData() {

    let loader = this.presentLoading();
    this.contatoProvider.listar(this.storage.getLocalUser().email)
      .subscribe(response => {
          this.items = response;
          loader.dismiss();
        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  detalhesContato(id: string) {
    this.navCtrl.push("DetalhesContatoPage",{id : id});
  }
  cadastrarContato(){
    this.navCtrl.push("AdicionarContatoPage");
  }
}
