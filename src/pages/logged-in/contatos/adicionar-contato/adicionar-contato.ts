import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EventoProvider} from "../../../../providers/evento/evento";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {StorageService} from "../../../../providers/storage/storage.service";

/**
 * Generated class for the AdicionarContatoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adicionar-contato',
  templateUrl: 'adicionar-contato.html',
})
export class AdicionarContatoPage {

  private formContato: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public contatoProvider : ContatoProvider,
    public storage : StorageService
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formContato = this.formBuilder.group({
      email : ["", Validators.required]
    });

  }

  adicionarContato() {
    let formValues = this.formContato.getRawValue();


    let {email} = formValues;
    let body = {email : email};
    let usuario = this.storage.getLocalUser().email;

    this.contatoProvider.adicionar(body, usuario).subscribe(response => {
      },
      error => {}
    );

    this.navCtrl.pop();
  }
}
