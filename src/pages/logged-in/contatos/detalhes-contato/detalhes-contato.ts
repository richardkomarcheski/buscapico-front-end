import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Contato} from "../../../../models/contato";
import {ContatoProvider} from "../../../../providers/contato/contato";
import {StorageService} from "../../../../providers/storage/storage.service";



@IonicPage()
@Component({
  selector: 'page-detalhes-contato',
  templateUrl: 'detalhes-contato.html',
})
export class DetalhesContatoPage {

  item: Contato;
  id : string = this.navParams.get('id');
  email : string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public contatoProvider: ContatoProvider,
    public alertCtrl: AlertController,
    public storage: StorageService) {
  }


  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
    this.carregarDetalhes();
  }



  excluirContato() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão do Contato',
      message: 'Você realmente deseja excluir esse Contato?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            let id = this.navParams.get('id');
            let email = this.storage.getLocalUser().email;
            this.contatoProvider.deleteById(email, id).subscribe(response => {
                this.item = response;
                this.navCtrl.pop();
              },
              error => {});
          }
        }
      ]
    });
    alert.present();
  }

  private carregarDetalhes() {
    let id = this.navParams.get('id');
    let email = this.storage.getLocalUser().email;
    this.contatoProvider.findById(email, id)
      .subscribe(response => {
          this.item = response;
        },
        error => {});
  }
}
