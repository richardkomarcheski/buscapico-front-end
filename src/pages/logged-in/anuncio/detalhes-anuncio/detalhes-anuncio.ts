import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";
import {Anuncio} from "../../../../models/anuncio";
import {Foto} from "../../../../models/foto";
import {StorageService} from "../../../../providers/storage/storage.service";
import {UsuarioProvider} from "../../../../providers/usuario/usuario";
import {Comentario} from "../../../../models/comentario";
import {Usuario} from "../../../../models/usuario";


@IonicPage()
@Component({
  selector: 'page-detalhes-anuncio',
  templateUrl: 'detalhes-anuncio.html',
})
export class DetalhesAnuncioPage {

  item: Anuncio;
  id: string = this.navParams.get('id');
  fotos : Foto[];
  comentario: Comentario;
  comentarios: Comentario[];
  usuario: Usuario;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public anuncioProvider: AnuncioProvider,
    public alertCtrl: AlertController,
    public storage: StorageService,
    public usuarioProvider: UsuarioProvider

  ) { this.carregarUsuario();
  }

  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
    this.carregarDetalhes();
  }

  carregarDetalhes() {
    let id = this.navParams.get('id');
    console.log(id);
    this.anuncioProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          this.comentarios = this.item.comentarios;
          this.fotos = this.item.fotos;
          console.log(response);
        },
        error => {
        });
  }

  editarAnuncio( anuncio : Anuncio, id : string) {
    this.navCtrl.push("EditarAnuncioPage", {anuncio : anuncio, id : id});
  }

  adicionarComentario(){
    let alert = this.alertCtrl.create({
      title: 'Adicionar Comentário',
      message: 'escreva seu comentário',
      inputs: [
        {
          name: 'texto',
          placeholder: 'Comentário'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: dataComentario => {
            this.carregarUsuario();
            this.comentario = dataComentario;
            this.comentario.usuario = this.usuario;
            this.comentario.data = new Date();
            console.log(this.comentario);
            this.anuncioProvider.inserirComentario(this.comentario, this.id).subscribe(
              response => {
                this.carregarDetalhes();
              },
              error => {
              }
            )
          }
        }
      ]
    });
    alert.present();
  }
  excluirAnuncio() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão do Anúncio',
      message: 'Você realmente deseja excluir esse Anúncio?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.anuncioProvider.deleteById(this.id).subscribe(response => {
                this.item = response;
                console.log(response);
                this.navCtrl.pop();
              },
              error => {});
          }
        }
      ]
    })
    alert.present();
  }

  private carregarUsuario() {
    this.usuarioProvider.findByEmail(this.storage.getLocalUser().email).subscribe(response => {
        let {id, nome, email} = response;
        this.usuario = null;
        this.usuario = {id, nome, email};
      },
      error => {
      });
  }
}
