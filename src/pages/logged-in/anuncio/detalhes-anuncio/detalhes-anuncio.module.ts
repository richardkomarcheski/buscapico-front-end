import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesAnuncioPage } from './detalhes-anuncio';

@NgModule({
  declarations: [
    DetalhesAnuncioPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesAnuncioPage),
  ],
})
export class DetalhesAnuncioPageModule {}
