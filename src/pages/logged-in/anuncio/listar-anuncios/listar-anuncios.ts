import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {CadastrarAnuncioPage} from "../cadastrar-anuncio/cadastrar-anuncio";
import  {Anuncio} from "../../../../models/anuncio";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";

@IonicPage()
@Component({
  selector: 'page-listar-anuncios',
  templateUrl: 'listar-anuncios.html',
})
export class ListarAnunciosPage {
  items : Anuncio [];
  page : number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl : LoadingController, public anucioProvider : AnuncioProvider) {
  }




  ionViewDidLoad() {
    this.loadData();
  }

  ionViewDidEnter(){
     this.loadData();
  }

  loadData() {

    let loader = this.presentLoading();
    this.anucioProvider.listar()
      .subscribe(response => {
          this.items = response;
          loader.dismiss();
          console.log(response);

        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  showDetail(id: string) {
    console.log(id);
    this.navCtrl.push("DetalhesAnuncioPage",{id : id});
  }

  adicionarAnuncio() {
    this.navCtrl.push("CadastrarAnuncioPage");
  }
}
