import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComentariosAnuncioPage } from './comentarios-anuncio';

@NgModule({
  declarations: [
    ComentariosAnuncioPage,
  ],
  imports: [
    IonicPageModule.forChild(ComentariosAnuncioPage),
  ],
})
export class ComentariosAnuncioPageModule {}
