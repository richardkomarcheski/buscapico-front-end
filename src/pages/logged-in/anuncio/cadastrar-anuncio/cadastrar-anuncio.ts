import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";
import {StorageService} from "../../../../providers/storage/storage.service";
import {Foto} from "../../../../models/foto";
import {UsuarioProvider} from "../../../../providers/usuario/usuario";
import {Usuario} from "../../../../models/usuario";


@IonicPage()
@Component({
  selector: 'page-cadastrar-anuncio',
  templateUrl: 'cadastrar-anuncio.html',
})
export class CadastrarAnuncioPage {

  private formAnuncio: FormGroup;
  private dataCriacao = new Date().toISOString();
  usuario : Usuario;
  fotos : Foto[];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public anuncioProvider: AnuncioProvider,
    public alertCtrl : AlertController,
    public storage : StorageService,
    public usuarioProvider : UsuarioProvider
  ) {
    this.carregarUsuario();
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formAnuncio = this.formBuilder.group({
      titulo: ["", Validators.required],
      preco: ["123.00", Validators.required],
      descricao: ["", Validators.required],
      dataCadastro: [],
      fotos: ["link da foto"],
      anunciante : []

    });

  }

  // addFoto(){
  //   let alert = this.alertCtrl.create({
  //     title: 'Adicionar Foto',
  //     message: 'coloque o link da foto',
  //     inputs: [
  //       {
  //         name: 'path',
  //         placeholder: 'Link'
  //       },
  //       {
  //         name: 'descricao',
  //         placeholder: 'descrição'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancelar',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Salvar',
  //         handler: dataFoto => {
  //         this.foto = (<Foto>dataFoto);
  //         let size = this.fotos.length;
  //         this.fotos[size+1] = this.foto;
  //         console.log(this.fotos);
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  // }

  adicionarAnuncio() {
    let formValues = this.formAnuncio.getRawValue();
    console.log(formValues);

    this.carregarUsuario();
    let {titulo, preco, descricao, dataCadastro, fotos, anunciante} = formValues;
    let body = {
      titulo,
      preco,
      descricao,
      dataCadastro,
      fotos: [{'path' :fotos.toString()}],
      anunciante
    };

    body.dataCadastro = this.dataCriacao;
    body.anunciante = this.usuario;

    console.log(body);

    this.anuncioProvider.adicionar(body).subscribe(response => {
      this.cadastroSucesso();
      },
      error => {

      }
    );
  }

  carregarUsuario() {
    this.usuarioProvider.findByEmail(this.storage.getLocalUser().email).subscribe(response => {
        let {id, nome, email} = response;
        this.usuario = null;
        this.usuario = {id, nome, email};
      },
      error => {
      });
  }

  cadastroSucesso() {
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Anúncio cadastrado com sucesso!',
      enableBackdropDismiss: false,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.pop();
        }
      }
      ]
    });
    alert.present();
  }
}
