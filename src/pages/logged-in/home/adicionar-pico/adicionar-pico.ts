import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Geolocation } from "@ionic-native/geolocation";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";

@IonicPage()
@Component({
  selector: "page-adicionar-pico",
  templateUrl: "adicionar-pico.html"
})
export class AdicionarPicoPage {
  private formPico: FormGroup;
  private localizacao;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public picoProvider: PicoProvider,
    public geolocation: Geolocation
  ) {}

  ngOnInit() {
    this.initForm();
  }


  ionViewDidLoad() {
    console.log("ionViewDidLoad AdicionarPicoPage");
  }


  private initForm() {
    this.formPico = this.formBuilder.group({
      nome: ["", Validators.required],
      descricao: ["",Validators.required],
      endereco: ["",Validators.required],
      numero: ["",Validators.required],
      entrada: [''],
      gratuito: ['']
    });
    this.geolocation.getCurrentPosition().then(position => {
      this.localizacao = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      };
    });
  }

  public adicionar() {
    let formValues = this.formPico.getRawValue();
    console.log(formValues);

    let { nome, descricao, entrada, gratuito, numero, endereco } = formValues;
    let body = {
      nome,
      descricao,
      entrada,
      gratuito,
      endereco: { rua: endereco, numero, localizacao: this.localizacao }
    };

    console.log("body", body);
    this.picoProvider.adicionar(body).subscribe(test => console.log(test)
      ,
      error => {console.log(error)}
    );

    this.navCtrl.pop();
  }
}
