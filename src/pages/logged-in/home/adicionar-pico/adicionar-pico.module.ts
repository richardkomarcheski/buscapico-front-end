import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarPicoPage } from './adicionar-pico';

@NgModule({
  declarations: [
    AdicionarPicoPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarPicoPage),
  ],
})
export class AdicionarPicoPageModule {}
