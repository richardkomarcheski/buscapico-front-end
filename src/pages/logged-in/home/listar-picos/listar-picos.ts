import { Component } from "@angular/core";
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams
} from "ionic-angular";
import { Pico } from "../../../../models/pico";
import { PicoProvider } from "../../../../providers/pico/pico.provider";

/**
 * Generated class for the ListarPicosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-listar-picos",
  templateUrl: "listar-picos.html"
})
export class ListarPicosPage {
  picos: Pico[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public picoProvider: PicoProvider
  ) {}

  ionViewDidEnter(){
    this.loadData();
  }
  ionViewDidLoad() {
    this.loadData();
  }

  loadData() {
    let loader = this.presentLoading();
    this.picoProvider.listar().subscribe(
      response => {
        this.picos = response;
        loader.dismiss();
            },
      error => {
        loader.dismiss();
      }
    );
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  goToAdicionarPico() {
    this.navCtrl.push("AdicionarPicoPage");
  }

  goToDetalhe(id: string) {
    this.navCtrl.push("DetalhePicoPage", {id : id});
  }
}
