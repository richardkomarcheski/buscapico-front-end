import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Pico} from "../../../../models/pico";
import {PicoProvider} from "../../../../providers/pico/pico.provider";

@IonicPage()
@Component({
  selector: 'page-detalhe-pico',
  templateUrl: 'detalhe-pico.html',
})
export class DetalhePicoPage {


  item : Pico ;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public picoProvider : PicoProvider    ) {
  }

  ionViewDidLoad() {
    let id = this.navParams.get('id');
    console.log(id);
    this.picoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          console.log(response);
        },
        error => {});
  }
}
