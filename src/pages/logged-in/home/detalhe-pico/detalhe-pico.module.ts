import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhePicoPage } from './detalhe-pico';

@NgModule({
  declarations: [
    DetalhePicoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhePicoPage),
  ],
})
export class DetalhePicoPageModule {}
