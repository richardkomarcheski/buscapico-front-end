import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {MutiraoProvider} from "../../../../providers/mutirao/mutirao";

/**
 * Generated class for the CadastrarMutiraoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastrar-mutirao',
  templateUrl: 'cadastrar-mutirao.html',
})
export class CadastrarMutiraoPage {
  private formMutirao : FormGroup;
  private dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public mutiraoProvider : MutiraoProvider,
    public alertCtrl : AlertController

  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formMutirao = this.formBuilder.group({

      dataMutirao : [,Validators.required],
      dataCadastro: [],

    });

  }

  cadastrarMutirao() {
    let formValues = this.formMutirao.getRawValue();
    console.log(formValues);

    let { dataMutirao , dataCadastro} = formValues;
    let body = {

      dataMutirao,
      dataCadastro
    };
    body.dataCadastro = this.dataCriacao;
       console.log("body", body);

    this.mutiraoProvider.adicionar(body).subscribe(response => {
        this.adicionarSucesso();
      },
      error => {}
    );
  }

   adicionarSucesso() {

     let alert = this.alertCtrl.create({
       title: 'Sucesso!',
       message: 'Mutirão cadastrado com sucesso!',
       enableBackdropDismiss: false,
       buttons: [{
         text: 'Ok',
         handler: () => {
           this.navCtrl.pop();
         }
       }
       ]
     });
     alert.present();
   }

}
