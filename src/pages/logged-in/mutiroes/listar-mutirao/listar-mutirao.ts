import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {PicoProvider} from "../../../../providers/pico/pico.provider";
import {Pico} from "../../../../models/pico";
import {Mutirao} from "../../../../models/mutirao";
import {MutiraoProvider} from "../../../../providers/mutirao/mutirao";

/**
 * Generated class for the ListarMutiraoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listar-mutirao',
  templateUrl: 'listar-mutirao.html',
})
export class ListarMutiraoPage {

  items : Mutirao [] ;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl : LoadingController,
              public mutiraoProvider : MutiraoProvider) {
  }

  ionViewDidLoad() {
    this.loadData();
  }

  ionViewDidEnter(){
    this.loadData();
  }


  loadData() {

    let loader = this.presentLoading();
    this.mutiraoProvider.listar()
      .subscribe(response => {
          this.items = response;
          loader.dismiss();
          console.log(response);

        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  adicionarMutirao() {
    this.navCtrl.push("CadastrarMutiraoPage");
  }

  detalheMutirao(id: string) {
    this.navCtrl.push("DetalhesMutiraoPage",{id : id});
  }
}
