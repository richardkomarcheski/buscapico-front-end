import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarMutiraoPage } from './listar-mutirao';

@NgModule({
  declarations: [
    ListarMutiraoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarMutiraoPage),
  ],
})
export class ListarMutiraoPageModule {}
