import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Anuncio} from "../../../../models/anuncio";
import {Mutirao} from "../../../../models/mutirao";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";
import {MutiraoProvider} from "../../../../providers/mutirao/mutirao";

/**
 * Generated class for the DetalhesMutiraoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes-mutirao',
  templateUrl: 'detalhes-mutirao.html',
})
export class DetalhesMutiraoPage {
  item: Mutirao;
  id: string = this.navParams.get('id');

  constructor( public navCtrl: NavController,
               public navParams: NavParams,
               public mutiraoProvider: MutiraoProvider,
               public alertCtrl: AlertController) {
  }
  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
    this.carregarDetalhes();
  }
  carregarDetalhes() {
    let id = this.navParams.get('id');
    console.log(id);
    this.mutiraoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          console.log(response);
        },
        error => {
        });
  }

  editarMutirao( mutirao : Mutirao, id : string) {
    this.navCtrl.push("EditarMutiraoPage", {mutirao : mutirao, id : id});
  }

  excluirMutirao()  {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão do Mutirão',
      message: 'Você realmente deseja excluir esse Mutirão?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.mutiraoProvider.deleteById(this.id).subscribe(response => {
                this.item = response;
                this.navCtrl.pop();
              },
              error => {});
          }
        }
      ]
    })
    alert.present();
  }
}
