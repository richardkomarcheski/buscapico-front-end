import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComentarioMutiraoPage } from './comentario-mutirao';

@NgModule({
  declarations: [
    ComentarioMutiraoPage,
  ],
  imports: [
    IonicPageModule.forChild(ComentarioMutiraoPage),
  ],
})
export class ComentarioMutiraoPageModule {}
