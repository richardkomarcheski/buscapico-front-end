import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {MutiraoProvider} from "../../../../providers/mutirao/mutirao";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Sessao} from "../../../../models/sessao";
import {Mutirao} from "../../../../models/mutirao";

/**
 * Generated class for the EditarMutiraoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-mutirao',
  templateUrl: 'editar-mutirao.html',
})
export class EditarMutiraoPage {
  item: Mutirao = this.navParams.get('mutirao');
  private dataCriacao = new Date().toISOString();
  private formMutirao : FormGroup;
  id : string = this.navParams.get('id');

  constructor(public navCtrl: NavController, public navParams: NavParams,  public formBuilder: FormBuilder,
              public mutiraoProvider : MutiraoProvider) {
  }

  ionViewDidLoad() {

  }

  ngOnInit() {
    this.initForm();
  }


  private initForm() {


    this.formMutirao = this.formBuilder.group({

      dataMutirao : [  ,Validators.required],
      dataCadastro: [],
    });


  }


  salvarMutirao() {
    let formValues = this.formMutirao.getRawValue();
    let { dataMutirao, dataCadastro} = formValues;
    let body = {

      dataMutirao,
      dataCadastro
    };
    body.dataCadastro = this.item.dataCadastro;

    this.mutiraoProvider.editar(body, this.id).subscribe(response => {
        this.navCtrl.pop();
      },
      error => {}
    );

  }
}
