import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarMutiraoPage } from './editar-mutirao';

@NgModule({
  declarations: [
    EditarMutiraoPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarMutiraoPage),
  ],
})
export class EditarMutiraoPageModule {}
