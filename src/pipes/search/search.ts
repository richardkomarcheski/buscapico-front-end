import { Pipe, PipeTransform } from '@angular/core';
import {Sessao} from "../../models/sessao";

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: Sessao[], terms: string): Sessao[] {
    terms;
    return items.filter( it => {
      return it.nome.toLowerCase().includes(terms);
    });
  }
}
