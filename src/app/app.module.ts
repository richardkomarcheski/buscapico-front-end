import { HttpClientModule } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { Geolocation } from "@ionic-native/geolocation";
import { NativeStorage } from "@ionic-native/native-storage";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SideMenuComponent } from "../components/side-menu/side-menu";
import { HttpConfigProvider } from "../providers/http-config/http-config";
import { NetworkProvider } from "../providers/network/network.provider";
import { PicoProvider } from "../providers/pico/pico.provider";
import { MyApp } from "./app.component";
import { IonicStorageModule } from "@ionic/storage";
import { AuthProvider } from '../providers/auth/auth.provider';
import { SessaoProvider } from '../providers/sessao/sessao';
import { AnuncioProvider } from '../providers/anuncio/anuncio';
import { MutiraoProvider } from '../providers/mutirao/mutirao';
import { EventoProvider } from '../providers/evento/evento';
import { FavoritoProvider } from '../providers/favorito/favorito';
import {AuthService} from "../providers/auth/auth.service";
import {UsuarioProvider} from "../providers/usuario/usuario";
import {StorageService} from "../providers/storage/storage.service";
import {ErrorInterceptorProvider} from "../interceptor/error-interceptor";
import {AuthInterceptorProvider} from "../interceptor/auth-interceptor";
import {ContatoProvider} from "../providers/contato/contato";
import {IonicImageViewerModule} from "ionic-img-viewer";
import {PipesModule} from "../pipes/pipes.module";

@NgModule({
  declarations: [MyApp, SideMenuComponent],
  imports: [
    BrowserModule,
    IonicImageViewerModule,
    HttpClientModule,
    PipesModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    PicoProvider,
    HttpConfigProvider,
    Geolocation,
    NetworkProvider,
    NativeStorage,
    AuthProvider,
    SessaoProvider,
    SessaoProvider,
    AnuncioProvider,
    PicoProvider,
    MutiraoProvider,
    EventoProvider,
    FavoritoProvider,
    AuthService,
    UsuarioProvider,
    StorageService,
    AuthInterceptorProvider,
    ErrorInterceptorProvider,
    ContatoProvider


  ]
})
export class AppModule {}
