import {Comentario} from "./comentario";
import {Usuario} from "./usuario";
import {Foto} from "./foto";

export  interface Anuncio {
  titulo : string;
  preco : number;
  descricao : string;
  dataCadastro : Date;
  fotos : Foto[];
  comentarios : Comentario[];
  anunciante : Usuario;
}
