import {Etapa} from "./etapa";
import {Foto} from "./foto";
import {Video} from "./video";
import {Comentario} from "./comentario";

export  interface  Evento {
  nome : string;
  etapas : Etapa[];
  dataCadastro : Date;
  fotos : Foto[];
  videos : Video[];
  comentarios : Comentario[];
}
