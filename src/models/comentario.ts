import {Usuario} from "./usuario";

export interface  Comentario {
  texto: string;
  data: Date;
  usuario : Usuario;
}
