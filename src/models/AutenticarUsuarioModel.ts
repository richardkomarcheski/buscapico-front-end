import { UsuarioModel } from "./UsuarioModel";

export class AutenticarUsuarioModel {
  usuario: UsuarioModel;
  jwt: string;
  refreshToken: string;
}
