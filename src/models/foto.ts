import {Usuario} from "./usuario";

export interface Foto {
  descricao: string,
  path: string,
  usuario: Usuario
}
