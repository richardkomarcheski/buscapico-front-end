import {Pico} from "./pico";
import {Video} from "./video";
import {Foto} from "./foto";
import {Comentario} from "./comentario";
import {Contato} from "./contato";

export interface Sessao {
  nome : string;
  pico: Pico;
  dataSessao: Date;
  dataCadastro: Date;
  videos : Video[];
  fotos : Foto[];
  comentarios : Comentario[];
  participantes : Contato[];
}
