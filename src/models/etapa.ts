import {Pico} from "./pico";
import {Contato} from "./contato";

export interface Etapa {
  data : Date;
  nome : string;
  pico : Pico;
  participantes : Contato[];
}
