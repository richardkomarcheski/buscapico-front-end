import {Usuario} from "./usuario";

export interface Video {
  descricao: string,
  urlYoutube: string,
  user: Usuario
}
